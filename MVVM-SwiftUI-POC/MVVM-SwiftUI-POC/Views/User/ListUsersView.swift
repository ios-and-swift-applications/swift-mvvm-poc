//
//  ListUsersView.swift
//  MVVM-SwiftUI-POC
//
//  Created by Jorge luis Menco Jaraba on 29/11/20.
//

import Foundation
import SwiftUI

struct ListUsersView: View {
    
    @ObservedObject var listUserViemodel: GetListUsersViewModel
    @ObservedObject var addUserViewModel: AddUserViewModel
    
    
    init(listUserViemodel: GetListUsersViewModel, addUserViewModel: AddUserViewModel) {
        self.listUserViemodel = listUserViemodel
        self.addUserViewModel = addUserViewModel
    }
    
    var body: some View {
        NavigationView {
            List {
                searchView
                
                if listUserViemodel.users.isEmpty {
                    Text("users not found")
                } else {
                    ForEach(listUserViemodel.users, id: \.self) { user in
                        UserCell(user: user.name)
                    }
                }
                
            }.navigationBarItems(trailing:
                addUserButoon)
            .navigationBarTitle("Users")
        }
    }
}

private extension ListUsersView {
    var searchView: some View {
        HStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: /*@START_MENU_TOKEN@*/nil/*@END_MENU_TOKEN@*/, content: {
            TextField("Search", text: $listUserViemodel.query)
            Button("search") {
                listUserViemodel.getUsersByName(name: listUserViemodel.query)
            }
        })
    }
    
    var addUserButoon: some View {
        NavigationLink(
            destination: AddUserView(viewModel: addUserViewModel),
            label: {
                Image(systemName: "plus.square.fill").font(.largeTitle)
            })
    }
}
