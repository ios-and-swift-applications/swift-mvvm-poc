//
//  AddUserForm.swift
//  MVVM-SwiftUI-POC
//
//  Created by Jorge luis Menco Jaraba on 1/12/20.
//

import Foundation
import SwiftUI

struct AddUserView: View {
    
    @ObservedObject var vieModel: AddUserViewModel
    
    init(viewModel: AddUserViewModel) {
        self.vieModel = viewModel
    }
    
    var body: some View {
        VStack {
            Form {
                Section (header:  Text("Personal information")) {
                    TextField("Name", text: $vieModel.user.name)
                    Button(action: {
                        vieModel.addUser()
                    }, label: {
                        Text("+ Add User")
                    })
                }
            }
        }
    }
}
