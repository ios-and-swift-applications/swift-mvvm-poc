//
//  UserCell.swift
//  MVVM-SwiftUI-POC
//
//  Created by Jorge luis Menco Jaraba on 29/11/20.
//

import Foundation
import SwiftUI

struct UserCell: View {
    
    private var user: String
    
    init(user: String) {
        self.user = user
    }
    
    var body: some View {
        HStack {
            Image("personProfile").frame(
                width: 100,
                height: 100,
                alignment: .leading)
                .scaledToFill()
            VStack(alignment: .leading, spacing: 5, content: {
                Text(user).font(.title)
                Text("Junior dev").font(.title2)
                Text("condorlabs.io").font(.title3)
            })
            
        }.lineSpacing(5)
        .padding(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/, /*@START_MENU_TOKEN@*/10/*@END_MENU_TOKEN@*/)
    }
}
