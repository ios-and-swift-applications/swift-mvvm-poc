//
//  UserRepository.swift
//  MVVM-SwiftUI-POC
//
//  Created by Jorge luis Menco Jaraba on 29/11/20.
//

import Foundation

protocol UserRepositoryType {
    func getUsersByName(name: String) -> [User]
    func addUser(user: User)
}

final class UserRepository: UserRepositoryType {
    
    var users: [User] = [
        User(iconName: "personProfile.png", name: "Maria"),
        User(iconName: "personProfile.png", name: "Josefa"),
        User(iconName: "personProfile.png", name: "Andrea"),
        User(iconName: "personProfile.png", name: "Miguel"),
        User(iconName: "personProfile.png", name: "Alavro"),
        User(iconName: "personProfile.png", name: "Heran"),
        User(iconName: "personProfile.png", name: "Carlos"),
        User(iconName: "personProfile.png", name: "Guillermo"),
        User(iconName: "personProfile.png", name: "Sergio"),
        User(iconName: "personProfile.png", name: "Felipe"),
        User(iconName: "personProfile.png", name: "Leydi"),
        User(iconName: "personProfile.png", name: "Oscar"),
        User(iconName: "personProfile.png", name: "Jesus"),
        User(iconName: "personProfile.png", name: "Jose"),
        User(iconName: "personProfile.png", name: "Miguel"),
        User(iconName: "personProfile.png", name: "Amapro"),
        User(iconName: "personProfile.png", name: "Liliana"),
        User(iconName: "personProfile.png", name: "Asdrual"),
        User(iconName: "personProfile.png", name: "Vanesa"),
        User(iconName: "personProfile.png", name: "Camilo"),
    ]
    
    func getUsersByName(name: String) -> [User] {
        return users.filter { (user) -> Bool in
            return user.name.contains(name)
        }
    }
    
    func addUser(user: User) {
        self.users.append(user)
    }
}
