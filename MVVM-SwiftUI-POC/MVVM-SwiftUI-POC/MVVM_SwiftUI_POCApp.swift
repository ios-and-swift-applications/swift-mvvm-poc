//
//  MVVM_SwiftUI_POCApp.swift
//  MVVM-SwiftUI-POC
//
//  Created by Jorge luis Menco Jaraba on 23/11/20.
//

import SwiftUI

@main
struct MVVM_SwiftUI_POCApp: App {
    var body: some Scene {
        WindowGroup {
            let repository = UserRepository()
            ListUsersView(listUserViemodel: GetListUsersViewModel(repository: repository), addUserViewModel: AddUserViewModel(repository: repository))
        }
    }
}
