//
//  ContentView.swift
//  MVVM-SwiftUI-POC
//
//  Created by Jorge luis Menco Jaraba on 23/11/20.
//

import SwiftUI
import Combine

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        let repository = UserRepository()
        ListUsersView(listUserViemodel: GetListUsersViewModel(repository: repository), addUserViewModel: AddUserViewModel(repository: repository))
    }
}
