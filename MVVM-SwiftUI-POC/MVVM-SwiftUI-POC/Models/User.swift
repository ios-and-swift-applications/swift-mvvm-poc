//
//  User.swift
//  MVVM-SwiftUI-POC
//
//  Created by Jorge luis Menco Jaraba on 29/11/20.
//

import Foundation

struct User: Hashable {
    
    var iconName: String
    var name: String
}

