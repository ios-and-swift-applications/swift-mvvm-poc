//
//  AddUserViewModel.swift
//  MVVM-SwiftUI-POC
//
//  Created by Jorge luis Menco Jaraba on 1/12/20.
//

import Foundation
import Combine

protocol AddUserViemodelType: ObservableObject {
    func addUser()
}

final class AddUserViewModel: AddUserViemodelType {
 
    @Published var user: User = User(iconName: "person.fill", name: "")
    var disposabled = Set<AnyCancellable>()
    
    private let repository: UserRepositoryType
    
    init(repository: UserRepositoryType) {
        self.repository = repository
    }
    
    func addUser() {
        repository.addUser(user: user)
    }
}
