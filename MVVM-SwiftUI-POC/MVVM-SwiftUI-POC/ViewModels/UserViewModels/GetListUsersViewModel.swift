//
//  GetListUsersViewModel.swift
//  MVVM-SwiftUI-POC
//
//  Created by Jorge luis Menco Jaraba on 29/11/20.
//

import Foundation
import Combine

protocol GetListUserViewModelType: ObservableObject {
    
    func getUsersByName(name: String)
    func getDetailUser() -> User
}

final class GetListUsersViewModel: GetListUserViewModelType {
    
    @Published var query: String = ""
    @Published var users: [User] = []
    var disposabled = Set<AnyCancellable>()
    
    private let repository: UserRepositoryType
    
    init(repository: UserRepositoryType) {
        self.repository = repository
        addQuerySubscription()
    }
    
    func getUsersByName(name: String) {
        users  = repository.getUsersByName(name: name)
    }
    
    func getDetailUser() -> User {
        User(iconName: "", name: "")
    }
    
    private func addQuerySubscription() {
        let scheduler: DispatchQueue = DispatchQueue(label: "GetListUsersViewModel")
        $query
            .dropFirst()
            .debounce(for: .seconds(0.5), scheduler: scheduler)
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { value in
                print(value)
                self.getUsersByName(name: value)
            }).store(in: &disposabled)
    }
}
